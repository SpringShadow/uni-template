import Vue from 'vue'
let uniPromise = {};
uniPromise.login = function() {
    let promise = new Promise((resolve, reject) => {
        uni.login({
            provider: 'weixin',
            success: (loginRes) => {
                if (loginRes) {
                    resolve(loginRes)
                } else {
                    resolve('')
                }
            },
            fail: () => {
                reject();
            }
        });
    })
    return promise
}
uniPromise.getUserInfo = function() {
    let promise = new Promise((resolve, reject) => {
        uni.getUserInfo({
            provider: 'weixin',
            lang: 'zh_CN',
            success: (infoRes) => {
                if (infoRes) {
                    resolve(infoRes)
                } else {
                    resolve('')
                }
            },
            fail: () => {
                reject()
            }
        })
    })
    return promise
}
uniPromise.getSystemInfo = function() {
    let promise = new Promise((resolve, reject) => {
        uni.getSystemInfo({
            success: (res) => {
                resolve(res)
            },
            fail: () => {
                reject()
            }
        });
    })
    return promise
}

Vue.prototype.$uniPromise = uniPromise;

Vue.prototype.$showToast = function(mes) {
	if (mes && typeof mes == 'string') {
		uni.showToast({
			title: mes,
			icon: 'none',
			duration: 2000
		});
	}
}