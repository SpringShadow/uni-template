import Vue from 'vue'
import App from './App'
// 配置项
require('./config/config.js');
Vue.prototype.$config = config
// 封装uni的一些方法
import './plugins/uniPromise.js'
// 过滤器
import './plugins/filter.js'
// 封装的storage
import './plugins/storage.js'
// uni-vuex
let vuexStore = require("@/store/$u.mixin.js");
Vue.mixin(vuexStore);
import store from '@/store';
// uview-ui
import './plugins/uview.js'

Vue.config.productionTip = false
App.mpType = 'app'

const app = new Vue({
    store,
    ...App
})
app.$mount()
