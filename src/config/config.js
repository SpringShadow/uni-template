// production   development
let env='development'
let baseUrl,appid
if(env === 'production'){
    console.log('生产环境')
    baseUrl='https://yypay.reoyoo.com';
    appid='wxb04b88bea84e84b9';

}else{
    console.log('开发环境')
    baseUrl='https://testyypay.reoyoo.com';
    appid='wxb6ce0cb854b4899a';
}
let config = {
    tokenKey:'X-Token',
	// 请求接口的地址
	baseUrl: baseUrl,
    // 小程序appid
    appid:appid,
	// 是否需要MD5加密数据
	isMd5: true,
}

module.exports = config;
