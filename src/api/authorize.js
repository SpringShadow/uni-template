import uniRequest from './index.js'
import Vue from 'vue'
//----------------------------------------------------------------授权相关部分
/**
 * 获取SessionKey
 * @appid  小程序appid
 * @code  登录获取的code
 * @return session_key
 */
function getSessionKeyByCode(data) {
    return uniRequest({
        url: '/Api/WeChatMini/GetSessionKey',
        method: 'post',
        data: data
    })

}

/**
 * 获取授权信息
 * @encryptedData  用户主动授权之后获取的encryptedData
 * @iv             用户主动授权之后获取的iv
 * @key            登录获取的session_key
 * @return         返回token
 */
function getTokenByWxEncryptedData(data) {
    return uniRequest({
        url: '/Api/WeChatMini/WxEncryptedData',
        method: 'post',
        data: data
    })

}

export {
    getSessionKeyByCode,
    getTokenByWxEncryptedData,
}
